﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SahaCraneAuction.Models;
using System.IO;
using System.Globalization;
using System.Data.Entity;
using System.Net.Mail;
using System.Net;

namespace Sahaauction.Controllers
{
    public class BackendController : Controller
    {
        sahaauction_beEntities db = new sahaauction_beEntities();
        System.Globalization.CultureInfo _culture = new System.Globalization.CultureInfo("en-US");
        //CultureInfo _culture = new CultureInfo("en");
        // GET: Backend
        public ActionResult Status_IMG(int id, int status)
        {
            try
            {
                var banner = db.banners.Find(id);
                if (banner != null)
                {
                    if (status == 1)
                    {
                        banner.status_view = 1;
                    }
                    else
                    {
                        banner.status_view = 0;
                    }
                    db.Entry(banner).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Status_IMG : " + ex.Message);
            }
        }
        public ActionResult Status_IMG_PARTNER(int id, int status)
        {
            try
            {
                var partner = db.partners.Find(id);
                if (partner != null)
                {
                    if (status == 1)
                    {
                        partner.status_view = 1;
                    }
                    else
                    {
                        partner.status_view = 0;
                    }
                    db.Entry(partner).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Status_IMG : " + ex.Message);
            }
        }
        public ActionResult Add_Banner(banner banner)
        {
            try
            {
                if (Request.Files.Count > 0 && Request.Files != null)
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        if (files.AllKeys[i] == "path")
                        {
                            string filename = Path.GetFileNameWithoutExtension(file.FileName);
                            string extension = Path.GetExtension(file.FileName);
                            filename = filename + DateTime.Now.ToString("_ddMMyy_HH-mm-ss") + extension;
                            file.SaveAs(Path.Combine(Server.MapPath("/Files/"), filename));
                            banner.path = "/Files/" + filename;
                            banner.date = DateTime.Now;
                        }
                        else if (files.AllKeys[i] == "URL_upload" && banner.URL_upload != null)
                        {
                            string filename = Path.GetFileNameWithoutExtension(file.FileName);
                            string extension = Path.GetExtension(file.FileName);
                            filename = filename + DateTime.Now.ToString("_ddMMyy_HH-mm-ss") + extension;
                            file.SaveAs(Path.Combine(Server.MapPath("/Files/"), filename));
                            banner.URL_upload = "/Files/" + filename;
                        }
                    }
                    banner.status_view = 0;
                    db.banners.Add(banner);
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data null", JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return Json("Add_Banner : " + ex.Message);
            }
        }
        public ActionResult Select_banner(int id)
        {
            try
            {
                var banner = db.banners.Where(x => x.idbanner == id).Select(x => new
                {
                    id = x.idbanner,
                    name = x.name,
                    URL = x.URL,
                }).FirstOrDefault();
                return Json(banner, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Select_auction : " + ex.Message);
            }
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit_banner(banner banner)
        {
            try
            {
                if (banner != null)
                {
                    var bn = db.banners.Find(banner.idbanner);
                    bn.name = banner.name;
                    bn.date = DateTime.Now;
                    if (banner.URL != null)
                    {
                        bn.URL = banner.URL;
                        bn.URL_upload = null;
                    }
                    else if (banner.URL_upload != null)
                    {
                        bn.URL = null;
                    }
                    if (banner.path != null || banner.URL_upload != null)
                    {
                        HttpFileCollectionBase files = Request.Files;
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];
                            if (files.AllKeys[i] == "path" && banner.path != null)
                            {
                                string filename = Path.GetFileNameWithoutExtension(file.FileName);
                                string extension = Path.GetExtension(file.FileName);
                                filename = filename + DateTime.Now.ToString("_ddMMyy_HH-mm-ss") + extension;
                                file.SaveAs(Path.Combine(Server.MapPath("/Files/"), filename));
                                bn.path = "/Files/" + filename;
                            }
                            else if (files.AllKeys[i] == "URL_upload" && banner.URL_upload != null)
                            {
                                string filename = Path.GetFileNameWithoutExtension(file.FileName);
                                string extension = Path.GetExtension(file.FileName);
                                filename = filename + DateTime.Now.ToString("_ddMMyy_HH-mm-ss") + extension;
                                file.SaveAs(Path.Combine(Server.MapPath("/Files/"), filename));
                                bn.URL_upload = "/Files/" + filename;
                            }

                        }
                    }
                    db.Entry(bn).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data null", JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return Json("Add_auction : " + ex.Message);
            }
        }
        public ActionResult Del_banner(int id)
        {
            try
            {
                if (id != 0)
                {
                    db.banners.Remove(db.banners.Find(id));
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("faile", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json("Del_banner : " + ex.Message);
            }
        }
        public ActionResult Table_Banner()
        {
            try
            {
                var banner = db.banners.Select(x => new
                {
                    id = x.idbanner,
                    name = x.name,
                    path = x.path,
                    status = x.status,
                    status_view = x.status_view,
                }).ToList();
                return Json(banner, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Table_banner : " + ex.Message);
            }
        }


        public ActionResult Add_car_highlights(car_highlights car_highlights)
        {
            try
            {
                if (Request.Files.Count > 0 && Request.Files != null)
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string filename = Path.GetFileNameWithoutExtension(file.FileName);
                        string extension = Path.GetExtension(file.FileName);
                        filename = filename + DateTime.Now.ToString("_ddMMyy_HH-mm-ss") + extension;
                        file.SaveAs(Path.Combine(Server.MapPath("/Files/"), filename));
                        car_highlights.path = "/Files/" + filename;
                        car_highlights.date = DateTime.Now;
                        db.car_highlights.Add(car_highlights);
                        db.SaveChanges();
                    }
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data null", JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return Json("Add_car_highlights : " + ex.Message);
            }
        }
        public ActionResult Del_car_highlights(int id)
        {
            try
            {
                if (id != 0)
                {
                    db.car_highlights.Remove(db.car_highlights.Find(id));
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("faile", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json("Del_car_highlights : " + ex.Message);
            }
        }
        public ActionResult Table_car_highlights()
        {
            try
            {
                var car_highlights = db.car_highlights.Select(x => new
                {
                    id = x.idcar_highlights,
                    name = x.name,
                    path = x.path,
                }).ToList();
                return Json(car_highlights, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Table_car_highlights : " + ex.Message);
            }
        }

        public ActionResult Add_partner(partner partner)
        {
            try
            {
                if (partner.path != null || partner.link != null)
                {

                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        if (files.AllKeys[i] == "path" && partner.path != null)
                        {
                            string filename = Path.GetFileNameWithoutExtension(file.FileName);
                            string extension = Path.GetExtension(file.FileName);
                            filename = filename + DateTime.Now.ToString("_ddMMyy_HH-mm-ss") + extension;
                            file.SaveAs(Path.Combine(Server.MapPath("/Files/"), filename));
                            partner.path = "/Files/" + filename;
                        }
                        else if (files.AllKeys[i] == "URL_upload" && partner.URL_upload != null)
                        {
                            string filename = Path.GetFileNameWithoutExtension(file.FileName);
                            string extension = Path.GetExtension(file.FileName);
                            filename = filename + DateTime.Now.ToString("_ddMMyy_HH-mm-ss") + extension;
                            file.SaveAs(Path.Combine(Server.MapPath("/Files/"), filename));
                            partner.URL_upload = "/Files/" + filename;
                        }
                    }
                    partner.date = DateTime.Now;
                    partner.status_view = 0;
                    db.partners.Add(partner);
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("กรุณาเลือก Upload image หรือ Link image", JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return Json("Add_partner : " + ex.Message);
            }
        }
        public ActionResult Select_partner(int id)
        {
            try
            {
                var partner = db.partners.Where(x => x.idpartner == id).Select(x => new
                {
                    id = x.idpartner,
                    link = x.link,
                    name = x.name,
                    URL = x.URL,
                }).FirstOrDefault();
                return Json(partner, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Select_partner : " + ex.Message);
            }
        }
        public ActionResult Edit_partner(partner partner)
        {
            try
            {
                if (partner != null || partner.link != null)
                {
                    var bn = db.partners.Find(partner.idpartner);
                    bn.name = partner.name;
                    bn.date = DateTime.Now;
                    if (partner.URL != null)
                    {
                        bn.URL = partner.URL;
                        bn.URL_upload = null;
                    }
                    else if (partner.URL_upload != null)
                    {
                        bn.URL = null;
                    }

                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        if (files.AllKeys[i] == "path" && partner.path != null)
                        {
                            string filename = Path.GetFileNameWithoutExtension(file.FileName);
                            string extension = Path.GetExtension(file.FileName);
                            filename = filename + DateTime.Now.ToString("_ddMMyy_HH-mm-ss") + extension;
                            file.SaveAs(Path.Combine(Server.MapPath("/Files/"), filename));
                            bn.path = "/Files/" + filename;
                            bn.link = null;
                        }
                        else if (files.AllKeys[i] == "URL_upload" && partner.URL_upload != null)
                        {
                            string filename = Path.GetFileNameWithoutExtension(file.FileName);
                            string extension = Path.GetExtension(file.FileName);
                            filename = filename + DateTime.Now.ToString("_ddMMyy_HH-mm-ss") + extension;
                            file.SaveAs(Path.Combine(Server.MapPath("/Files/"), filename));
                            bn.URL_upload = "/Files/" + filename;
                        }
                    }

                    if (partner.link != null)
                    {
                        bn.link = partner.link;
                        bn.path = null;
                    }
                    db.Entry(bn).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data null", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("Add_auction : " + ex.Message);
            }
        }
        public ActionResult Del_partner(int id)
        {
            try
            {
                if (id != 0)
                {
                    db.partners.Remove(db.partners.Find(id));
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("faile", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json("Del_partner : " + ex.Message);
            }
        }
        public ActionResult Table_partner()
        {
            try
            {
                var partner = db.partners.Select(x => new
                {
                    id = x.idpartner,
                    name = x.name,
                    path = x.path,
                    link = x.link,
                    URL = x.URL,
                    status_view = x.status_view,
                }).ToList();
                return Json(partner, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Table_partner : " + ex.Message);
            }
        }

        public ActionResult Add_auction(auction auction)
        {
            try
            {
                if (Request.Files.Count > 0 && Request.Files != null)
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string filename = Path.GetFileNameWithoutExtension(file.FileName);
                        string extension = Path.GetExtension(file.FileName);
                        filename = filename + DateTime.Now.ToString("_ddMMyy_HH-mm-ss") + extension;
                        file.SaveAs(Path.Combine(Server.MapPath("/Files/"), filename));
                        auction.path = "/Files/" + filename;
                        db.auctions.Add(auction);
                        db.SaveChanges();
                    }
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data null", JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return Json("Add_auction : " + ex.Message);
            }
        }
        public ActionResult Select_auction(int id)
        {
            try
            {
                var auction = db.auctions.Where(x => x.idauction == id).Select(x => new
                {
                    id = x.idauction,
                    type_auction = x.type_auction,
                    path = x.path,
                    date = x.date,
                }).FirstOrDefault();
                return Json(auction, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Select_auction : " + ex.Message);
            }
        }
        public ActionResult Edit_auction(auction auction)
        {
            try
            {
                if (auction != null)
                {
                    var au = db.auctions.Find(auction.idauction);
                    au.type_auction = auction.type_auction;
                    if (auction.date != null)
                    {
                        au.date = auction.date;
                    }

                    if (auction.path != null)
                    {
                        HttpFileCollectionBase files = Request.Files;
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];
                            string filename = Path.GetFileNameWithoutExtension(file.FileName);
                            string extension = Path.GetExtension(file.FileName);
                            filename = filename + DateTime.Now.ToString("_ddMMyy_HH-mm-ss") + extension;
                            file.SaveAs(Path.Combine(Server.MapPath("/Files/"), filename));
                            au.path = "/Files/" + filename;
                        }
                    }
                    db.Entry(au).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data null", JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return Json("Add_auction : " + ex.Message);
            }
        }
        public ActionResult Del_auction(int id)
        {
            try
            {
                if (id != 0)
                {
                    db.auctions.Remove(db.auctions.Find(id));
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("faile", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json("Del_auction : " + ex.Message);
            }
        }
        public ActionResult Table_auction()
        {
            try
            {
                var auction = db.auctions.Select(x => new
                {
                    id = x.idauction,
                    type_auction = x.type_auction,
                    path = x.path,
                    date = x.date,
                }).ToList();
                return Json(auction, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Table_auction : " + ex.Message);
            }
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Add_calendar(calendar ca)
        {
            try
            {
                if (ca.topic != null)
                {
                    var text = Request["date"].ToString();
                    ca.date = Convert.ToDateTime(text, _culture);
                    db.calendars.Add(ca);
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data Null", JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return Json("Add_calendar : " + ex.Message);
            }
        }
        public ActionResult Select_calendar(int id)
        {
            try
            {
                var cal = db.calendars.Where(x => x.idcalendar == id).Select(x => new
                {
                    id = x.idcalendar,
                    status = x.status,
                    topic = x.topic,
                    textarea = x.textarea,
                    date = x.date
                }).FirstOrDefault();

                return Json(cal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Select_calendar : " + ex.Message);
            }

        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit_calendar(calendar ca)
        {
            try
            {


                var calendar = db.calendars.Find(ca.idcalendar);
                if (calendar != null)
                {
                    var s_date = Request["date"];
                    DateTime date = Convert.ToDateTime(s_date);

                    calendar.topic = ca.topic;
                    calendar.textarea = ca.textarea;
                    calendar.status = ca.status;
                    if (ca.date != null)
                    {
                        var text = Request["date"].ToString();
                        calendar.date = Convert.ToDateTime(text, _culture);
                        //calendar.date = Convert.ToDateTime(ca.date.ToString(),_culture);
                    }
                    db.Entry(calendar).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data Null", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("Edit_calendar : " + ex.Message);
            }
        }
        public ActionResult Del_calendar(int id)
        {
            try
            {
                if (id != 0)
                {
                    db.calendars.Remove(db.calendars.Find(id));
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("faile", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json("Del_calendar : " + ex.Message);
            }
        }
        public ActionResult Table_calendar()
        {
            try
            {
                var calendar = db.calendars.Select(x => new
                {
                    id = x.idcalendar,
                    topic = x.topic,
                    status = x.status,
                    date = x.date
                }).ToList();
                return Json(calendar, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Table_calendar : " + ex.Message);
            }
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Add_about_Head(about_head about)
        {
            try
            {
                if (about != null)
                {
                    if (about.status == 1)
                    {
                        var ab = db.about_head.Where(x => x.status == 1).FirstOrDefault();
                        if (ab != null)
                        {
                            ab.date = DateTime.Now;
                            ab.topic = about.topic;
                            ab.textarea = about.textarea;
                            db.Entry(ab).State = EntityState.Modified;
                        }
                        else
                        {
                            about.date = DateTime.Now;
                            db.about_head.Add(about);
                        }

                        db.SaveChanges();
                    }
                    else
                    {
                        if (about.path != null)
                        {
                            HttpFileCollectionBase files = Request.Files;
                            for (int i = 0; i < files.Count; i++)
                            {
                                HttpPostedFileBase file = files[i];
                                string filename = Path.GetFileNameWithoutExtension(file.FileName);
                                string extension = Path.GetExtension(file.FileName);
                                filename = filename + DateTime.Now.ToString("_ddMMyy_HH-mm-ss") + extension;
                                file.SaveAs(Path.Combine(Server.MapPath("/Files/"), filename));
                                about.path = "/Files/" + filename;
                                about.date = DateTime.Now;
                                about.status = 2;
                                db.about_head.Add(about);
                                db.SaveChanges();
                            }
                        }
                    }

                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data Null", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("Add_about : " + ex.Message);
            }
        }
        public ActionResult Select_about_head(int id)
        {
            try
            {
                var cal = db.about_head.Where(x => x.idabout_head == id).Select(x => new
                {
                    id = x.idabout_head,
                    topic = x.topic,
                    textarea = x.textarea,
                    path = x.path,
                    status = x.status,
                    date = x.date
                }).FirstOrDefault();

                return Json(cal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Select_about : " + ex.Message);
            }

        }
        public ActionResult Select_about_head_img()
        {
            try
            {
                var cal = db.about_head.Where(x => x.status == 2).Select(x => new
                {
                    id = x.idabout_head,
                    path = x.path,
                    status = x.status,
                    date = x.date
                }).ToList();

                return Json(cal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Select_about : " + ex.Message);
            }

        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit_about_head(about ca)
        {
            try
            {
                var about = db.about_head.Find(ca.idabout);
                if (about != null)
                {
                    about.topic = ca.topic;
                    about.textarea = ca.textarea;
                    about.date = DateTime.Now;
                    db.Entry(about).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data Null", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("Edit_about : " + ex.Message);
            }
        }
        public ActionResult Del_about_head(int id)
        {
            try
            {
                if (id != 0)
                {
                    db.about_head.Remove(db.about_head.Find(id));
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("faile", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json("Del_about : " + ex.Message);
            }
        }
        public ActionResult Del_about_head_IMG()
        {
            try
            {
                var img = db.about_head.Where(x => x.status == 2).ToList();
                if (img.Count != 0)
                {
                    db.about_head.RemoveRange(img);
                    db.SaveChanges();
                }
                return Json("success", JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json("Del_about : " + ex.Message);
            }
        }
        public ActionResult Del_about_head_IMG1(int id)
        {
            try
            {
                var img = db.about_head.Where(x => x.idabout_head == id).FirstOrDefault();
                db.about_head.Remove(img);
                db.SaveChanges();
                return Json("success", JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json("Del_about : " + ex.Message);
            }
        }
        public ActionResult Table_about_head()
        {
            try
            {
                var about = db.about_head.OrderByDescending(x => x.date).Select(x => new
                {
                    id = x.idabout_head,
                    topic = x.topic,
                    textarea = x.textarea,
                    path = x.path,
                    status = x.status,
                    date = x.date
                }).ToList();
                return Json(about, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Table_about : " + ex.Message);
            }
        }


        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Add_about(about about)
        {
            try
            {
                if (about.topic != null)
                {
                    about.date = DateTime.Now;
                    db.abouts.Add(about);
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data Null", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("Add_about : " + ex.Message);
            }
        }
        public ActionResult Select_about(int id)
        {
            try
            {
                var cal = db.abouts.Where(x => x.idabout == id).Select(x => new
                {
                    id = x.idabout,
                    topic = x.topic,
                    textarea = x.textarea,
                    date = x.date
                }).FirstOrDefault();

                return Json(cal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Select_about : " + ex.Message);
            }

        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit_about(about ca)
        {
            try
            {
                var about = db.abouts.Find(ca.idabout);
                if (about != null)
                {
                    about.topic = ca.topic;
                    about.textarea = ca.textarea;
                    about.date = DateTime.Now;
                    db.Entry(about).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data Null", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("Edit_about : " + ex.Message);
            }
        }
        public ActionResult Del_about(int id)
        {
            try
            {
                if (id != 0)
                {
                    db.abouts.Remove(db.abouts.Find(id));
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("faile", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json("Del_about : " + ex.Message);
            }
        }
        public ActionResult Table_about()
        {
            try
            {
                var about = db.abouts.Select(x => new
                {
                    id = x.idabout,
                    topic = x.topic,
                    date = x.date
                }).ToList();
                return Json(about, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Table_about : " + ex.Message);
            }
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Add_howto(howto howto)
        {
            try
            {
                if (howto.topic != null)
                {
                    howto.date = DateTime.Now;
                    db.howtoes.Add(howto);
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data Null", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("Add_howto : " + ex.Message);
            }
        }
        public ActionResult Select_howto(int id)
        {
            try
            {
                var cal = db.howtoes.Where(x => x.idhowto == id).Select(x => new
                {
                    id = x.idhowto,
                    topic = x.topic,
                    textarea = x.textarea,
                    date = x.date
                }).FirstOrDefault();

                return Json(cal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Select_howto : " + ex.Message);
            }

        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit_howto(howto ca)
        {
            try
            {
                var howto = db.howtoes.Find(ca.idhowto);
                if (howto != null)
                {
                    howto.topic = ca.topic;
                    howto.textarea = ca.textarea;
                    howto.date = DateTime.Now;
                    db.Entry(howto).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data Null", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("Edit_howto : " + ex.Message);
            }
        }
        public ActionResult Del_howto(int id)
        {
            try
            {
                if (id != 0)
                {
                    db.howtoes.Remove(db.howtoes.Find(id));
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("faile", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json("Del_howto : " + ex.Message);
            }
        }
        public ActionResult Table_howto()
        {
            try
            {
                var howto = db.howtoes.Select(x => new
                {
                    id = x.idhowto,
                    topic = x.topic,
                    date = x.date
                }).ToList();
                return Json(howto, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Table_howto : " + ex.Message);
            }
        }


        public ActionResult Add_news(news news)
        {
            try
            {
                if (Request.Files.Count > 0 && Request.Files != null)
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        if (files.AllKeys[i] == "path_topic")
                        {
                            string filename = Path.GetFileNameWithoutExtension(file.FileName);
                            string extension = Path.GetExtension(file.FileName);
                            filename = filename + DateTime.Now.ToString("_ddMMyy_HH-mm-ss") + extension;
                            file.SaveAs(Path.Combine(Server.MapPath("/Files/"), filename));
                            news.path = "/Files/" + filename;
                            db.news.Add(news);
                            db.SaveChanges();
                        }
                        else
                        {
                            n_content content = new n_content();
                            string filename = Path.GetFileNameWithoutExtension(file.FileName);
                            string extension = Path.GetExtension(file.FileName);
                            filename = filename + DateTime.Now.ToString("_ddMMyy_HH-mm-ss") + extension;
                            file.SaveAs(Path.Combine(Server.MapPath("/Files/"), filename));
                            content.path = "/Files/" + filename;
                            content.id_news = news.idnews;
                            db.n_content.Add(content);
                            db.SaveChanges();
                        }
                    }
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data null", JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return Json("Add_news : " + ex.Message);
            }
        }
        public ActionResult Select_news(int id)
        {
            try
            {
                var cal = db.news.Where(x => x.idnews == id).Select(x => new
                {
                    id = x.idnews,
                    topic = x.topic,
                    date = x.date
                }).FirstOrDefault();

                var content = db.n_content.Where(x => x.id_news == id).Select(x => new
                {
                    id = x.idcontent,
                    id_news = id,
                    path = x.path,
                }).ToList();

                return Json(new { cal, content }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Select_news : " + ex.Message);
            }
        }
        public ActionResult Del_content(int id)
        {
            try
            {
                if (id != 0)
                {
                    db.n_content.Remove(db.n_content.Find(id));
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("faile", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json("Del_news : " + ex.Message);
            }
        }

        public ActionResult Edit_news(news ca)
        {
            try
            {
                var news = db.news.Find(ca.idnews);
                if (news != null)
                {
                    news.topic = ca.topic;

                    if (ca.date != null)
                    {
                        news.date = ca.date;
                    }
                    db.Entry(news).State = EntityState.Modified;
                    db.SaveChanges();

                    if (Request.Files.Count > 0 && Request.Files != null)
                    {
                        HttpFileCollectionBase files = Request.Files;
                        //for (var f = 0; f < files.Count; f++)
                        //{
                        //    if (files.AllKeys[f] == "path_content")
                        //    {
                        //        db.n_content.RemoveRange(db.n_content.Where(x => x.id_news == news.idnews));
                        //        db.SaveChanges();
                        //        break;
                        //    }
                        //}


                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];
                            if (files.AllKeys[i] == "path_topic")
                            {
                                string filename = Path.GetFileNameWithoutExtension(file.FileName);
                                string extension = Path.GetExtension(file.FileName);
                                filename = filename + DateTime.Now.ToString("_ddMMyy_HH-mm-ss") + extension;
                                file.SaveAs(Path.Combine(Server.MapPath("/Files/"), filename));
                                news.path = "/Files/" + filename;
                                db.Entry(news).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                n_content content = new n_content();
                                string filename = Path.GetFileNameWithoutExtension(file.FileName);
                                string extension = Path.GetExtension(file.FileName);
                                filename = filename + DateTime.Now.ToString("_ddMMyy_HH-mm-ss") + extension;
                                file.SaveAs(Path.Combine(Server.MapPath("/Files/"), filename));
                                content.path = "/Files/" + filename;
                                content.id_news = news.idnews;
                                db.n_content.Add(content);
                                db.SaveChanges();
                            }
                        }
                    }

                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data Null", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("Edit_news : " + ex.Message);
            }
        }
        public ActionResult Del_news(int id)
        {
            try
            {
                if (id != 0)
                {
                    var n_content = db.n_content.Where(x => x.id_news == id).ToList();
                    foreach (var n in n_content)
                    {
                        db.n_content.Remove(db.n_content.Find(n.idcontent));
                        db.SaveChanges();
                    }

                    db.news.Remove(db.news.Find(id));
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("faile", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json("Del_news : " + ex.Message);
            }
        }
        public ActionResult Table_news()
        {
            try
            {
                var news = db.news.Select(x => new
                {
                    id = x.idnews,
                    topic = x.topic,
                    date = x.date
                }).ToList();
                return Json(news, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Table_news : " + ex.Message);
            }
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Add_contact(contact contact)
        {
            try
            {
                if (Request.Files.Count > 0 && Request.Files != null)
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];

                        string filename = Path.GetFileNameWithoutExtension(file.FileName);
                        string extension = Path.GetExtension(file.FileName);
                        filename = filename + DateTime.Now.ToString("_ddMMyy_HH-mm-ss") + extension;
                        file.SaveAs(Path.Combine(Server.MapPath("/Files/"), filename));
                        contact.path = "/Files/" + filename;
                        db.contacts.Add(contact);
                        db.SaveChanges();
                    }
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data null", JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return Json("Add_news : " + ex.Message);
            }
        }
        public ActionResult Select_contact(int id)
        {
            try
            {
                var contact = db.contacts.Where(x => x.idcontact == id).Select(x => new
                {
                    id = x.idcontact,
                    office = x.office,
                    tel = x.tel,
                    fax = x.fax,
                    email = x.email,
                    address = x.address,
                    path = x.path,
                    google_map = x.google_map,
                    status = x.status,
                    zone = x.zone,
                }).FirstOrDefault();

                return Json(contact, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Select_contact : " + ex.Message);
            }

        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit_contact(contact contact)
        {
            try
            {
                var co = db.contacts.Find(contact.idcontact);
                if (co != null)
                {
                    co.zone = contact.zone;
                    co.office = contact.office;
                    co.tel = contact.tel;
                    co.fax = contact.fax;
                    co.email = contact.email;
                    co.address = contact.address;
                    co.google_map = contact.google_map;
                    if (Request.Files.Count > 0 && Request.Files != null && contact.path != null)
                    {
                        HttpFileCollectionBase files = Request.Files;
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];

                            string filename = Path.GetFileNameWithoutExtension(file.FileName);
                            string extension = Path.GetExtension(file.FileName);
                            filename = filename + DateTime.Now.ToString("_ddMMyy_HH-mm-ss") + extension;
                            file.SaveAs(Path.Combine(Server.MapPath("/Files/"), filename));
                            co.path = "/Files/" + filename;
                        }
                    }

                    db.Entry(co).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data Null", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("Edit_about : " + ex.Message);
            }
        }
        public ActionResult Del_contact(int id)
        {
            try
            {
                if (id != 0)
                {
                    db.contacts.Remove(db.contacts.Find(id));
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("faile", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json("Del_news : " + ex.Message);
            }
        }
        public ActionResult Table_contact()
        {
            try
            {
                var news = db.contacts.Select(x => new
                {
                    id = x.idcontact,
                    office = x.office,
                    address = x.address,
                    status = x.status,
                    path = x.path,
                }).ToList();
                return Json(news, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Table_news : " + ex.Message);
            }
        }


        public ActionResult SignIn_Backend(account ac)
        {
            try
            {
                var message = 0;
                var username = db.accounts.Where(x => x.username == ac.username).FirstOrDefault();
                if (username != null)
                {
                    var password = db.accounts.Where(x => x.password == ac.password).FirstOrDefault(); ;
                    if (password != null)
                    {
                        RuntimeVariable.loginuser = username;
                        message = 1;
                    }
                    else
                    {
                        //pass fail
                        message = 2;
                    }
                }
                else
                {
                    //user fail
                    message = 3;
                }
                return Json(message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("SignIn_Backend : " + ex.Message);
            }
        }
        public ActionResult Add_Admin(account account)
        {
            try
            {
                var username = db.accounts.Where(x => x.username == account.username).FirstOrDefault();
                if (username == null)
                {
                    db.accounts.Add(account);
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data != null", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json("Add_Admin : " + ex.Message);
            }
        }
        public ActionResult Data_Edit_Admin(int id)
        {
            try
            {
                var account = db.accounts.Where(x => x.idaccount == id).Select(x => new
                {
                    id = x.idaccount,
                    username = x.username,
                    password = x.password,
                    firstname = x.firstname,
                    lastname = x.lastname,
                    email = x.email
                }).FirstOrDefault();
                return Json(account, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Edti_Ourservice : " + ex.Message);

            }
        }
        public ActionResult Edit_Admin(account account)
        {
            try
            {
                var user = db.accounts.Where(x => x.username == account.username && x.idaccount != account.idaccount).FirstOrDefault();
                if (user == null)
                {
                    var acc = db.accounts.Find(account.idaccount);
                    acc.username = account.username;
                    acc.password = account.password;
                    acc.firstname = account.firstname;
                    acc.lastname = account.lastname;
                    acc.email = account.email;
                    db.Entry(acc).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Data null", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("Add_Ourservice : " + ex.Message);
            }
        }
        public ActionResult Del_Admin(int id)
        {
            try
            {
                if (id != 0)
                {
                    db.accounts.Remove(db.accounts.Find(id));
                    db.SaveChanges();
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("faile", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json("Del_car_highlights : " + ex.Message);
            }
        }
        public ActionResult Table_Admin()
        {
            try
            {
                var account = db.accounts.Select(x => new
                {
                    id = x.idaccount,
                    username = x.username,
                    password = x.password,
                    firstname = x.firstname,
                    lastname = x.lastname,
                    email = x.email,
                    position = x.position,
                }).ToList();
                return Json(account, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Edti_Ourservice : " + ex.Message);

            }
        }

        public ActionResult Banner()
        {
            if (RuntimeVariable.loginuser != null)
            {
                return View();
            }
            return RedirectToAction("_SignIn");
        }
        public ActionResult Banner_mini()
        {
            if (RuntimeVariable.loginuser != null)
            {
                return View();
            }
            return RedirectToAction("_SignIn");
        }
        public ActionResult Calendar()
        {
            if (RuntimeVariable.loginuser != null)
            {
                return View();
            }
            return RedirectToAction("_SignIn");
        }
        public ActionResult About()
        {
            if (RuntimeVariable.loginuser != null)
            {
                return View();
            }
            return RedirectToAction("_SignIn");
        }
        public ActionResult Howto()
        {
            if (RuntimeVariable.loginuser != null)
            {
                return View();
            }
            return RedirectToAction("_SignIn");
        }
        public ActionResult News()
        {
            if (RuntimeVariable.loginuser != null)
            {
                return View();
            }
            return RedirectToAction("_SignIn");
        }
        public ActionResult Contact()
        {
            if (RuntimeVariable.loginuser != null)
            {
                return View();
            }
            return RedirectToAction("_SignIn");
        }
        public ActionResult Car_highlights()
        {
            if (RuntimeVariable.loginuser != null)
            {
                return View();
            }
            return RedirectToAction("_SignIn");
        }
        public ActionResult Partner()
        {
            if (RuntimeVariable.loginuser != null)
            {
                return View();
            }
            return RedirectToAction("_SignIn");
        }
        public ActionResult Auction()
        {
            if (RuntimeVariable.loginuser != null)
            {
                return View();
            }
            return RedirectToAction("_SignIn");
        }
        public ActionResult About_Head()
        {
            if (RuntimeVariable.loginuser != null)
            {
                return View();
            }
            return RedirectToAction("_SignIn");
        }
        public ActionResult _SignIn()
        {
            return View();
        }
        public ActionResult System_Admin()
        {
            if (RuntimeVariable.loginuser != null)
            {
                return View();
            }
            return RedirectToAction("_SignIn");
        }
        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("_SignIn");
        }

        public ActionResult SendEmail(Email1 contact)
        {
            try
            {
                string senderEmail = System.Configuration.ConfigurationManager.AppSettings["SenderEmail"].ToString();
                string senderPassword = System.Configuration.ConfigurationManager.AppSettings["SenderPassword"].ToString();
                string Smtp = System.Configuration.ConfigurationManager.AppSettings["Smtp"].ToString();
                int SmtpPort = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["SmtpPort"]);
                SmtpClient client = new SmtpClient(Smtp, SmtpPort);
                client.EnableSsl = true;
                client.Timeout = 100000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(senderEmail, senderPassword);


                MailMessage mailMessage = new MailMessage(contact.email, senderEmail, contact.firstname_lastname + " ติดต่อถึงบริษัท SAHA CRANE AUCTION CO.,LTD", contact.content_detail + "<p><font> เบอร์ติดต่อ : " + contact.tel + "</font></p>");

                if (contact.email != "")
                {
                    mailMessage.CC.Add(contact.email);
                    mailMessage.CC.Add(senderEmail);
                }

                mailMessage.IsBodyHtml = true;
                mailMessage.BodyEncoding = System.Text.Encoding.UTF8;
                //UTF8Encoding.UTF8
                client.Send(mailMessage);


                return Json("success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("SendEmail : " + ex.Message);
            }
        }

        public ActionResult ChangePosition(int id, int status)
        {
            try
            {
                var user = db.accounts.Find(id);
                var s_arr = user.position.Split(',');
                var Bool = true;
                Stack<string> n_arr = new Stack<string>();
                if (s_arr[0] != "")
                {
                    foreach (var i in s_arr)
                    {
                        var s = Convert.ToInt32(i);
                        if (status == 1)
                        {
                            if (s == status)
                            {
                                Bool = false;
                            }
                            else
                            {
                                n_arr.Push(i);
                            }
                        }
                        if (status == 2)
                        {
                            if (s == status)
                            {
                                Bool = false;
                            }
                            else
                            {
                                n_arr.Push(i);
                            }
                        }
                        if (status == 3)
                        {
                            if (s == status)
                            {
                                Bool = false;
                            }
                            else
                            {
                                n_arr.Push(i);
                            }
                        }
                        if (status == 4)
                        {
                            if (s == status)
                            {
                                Bool = false;
                            }
                            else
                            {
                                n_arr.Push(i);
                            }
                        }
                        if (status == 5)
                        {
                            if (s == status)
                            {
                                Bool = false;
                            }
                            else
                            {
                                n_arr.Push(i);
                            }
                        }
                    }
                }
                if (Bool)
                {
                    n_arr.Push(status.ToString());
                }
                var status_position = string.Join(",", n_arr);
                user.position = status_position;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("ChangePosition : " + ex.Message);
            }
        }

        public ActionResult CheckUser()
        {
            try
            {
                if (RuntimeVariable.loginuser != null)
                {
                    var id = RuntimeVariable.loginuser.idaccount;
                    var user = db.accounts.Where(x => x.idaccount == id).FirstOrDefault();
                    return Json(user, JsonRequestBehavior.AllowGet);
                }
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("CheckUser : " + ex.Message);
            }
        }
    }
}