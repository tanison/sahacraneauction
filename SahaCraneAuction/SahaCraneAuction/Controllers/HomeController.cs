﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SahaCraneAuction.Models;
using System.IO;
using System.Globalization;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using Org.BouncyCastle.Ocsp;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Sahaauction.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        sahaauction_beEntities db = new sahaauction_beEntities();
        public ActionResult Index()
        {
            HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(string.Format("http://devzt.devzt.me:81/Service/Get/auctionlist.asp"));
            WebReq.Method = "GET";

            HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

            Console.WriteLine(WebResp.StatusCode);
            Console.WriteLine(WebResp.Server);

            string jsonString;
            using (Stream stream = WebResp.GetResponseStream())
            {
                StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8);
                jsonString = reader.ReadToEnd();
            }
            JSONResponse.carauction response = JsonConvert.DeserializeObject<JSONResponse.carauction>(jsonString);

            var banner = db.banners.ToList();
            var partner = db.partners.ToList();
            //var auction = db.auctions.OrderByDescending(x => x.date).ToList();

            var status_partner = 0;
            foreach (var i in partner)
            {
                status_partner++;
            }

            ViewBag.status_partner = status_partner;
            ViewBag.auction = response.data;
            ViewBag.partner = partner;
            ViewBag.banner = banner;
            return View();
        }
        public ActionResult about()
        {
            var about = db.abouts.ToList();
            ViewBag.about = about;
            return View();
        }
        public ActionResult auction_login()
        {
            return View();
        }
        public ActionResult contact()
        {
            return View();
        }
        public ActionResult Event()
        {
            return View();
        }
        public ActionResult howto()
        {
            return View();
        }
        public ActionResult news_inside()
        {
            return View();
        }
        public ActionResult news()
        {
            return View();
        }
        public ActionResult register()
        {
            return View();
        }
        public ActionResult search()
        {
            return View();
        }
        public ActionResult carauction()
        {
            //var auli_auctionid = Convert.ToInt32(Request["auli_auctionid"]);
            //HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(string.Format("http://devzt.devzt.me:81/Service/Get/auctionlist.asp"));
            //WebReq.Method = "GET";

            //HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

            //Console.WriteLine(WebResp.StatusCode);
            //Console.WriteLine(WebResp.Server);

            //string jsonString;
            //using (Stream stream = WebResp.GetResponseStream())
            //{
            //    StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8);
            //    jsonString = reader.ReadToEnd();
            //}
            //JSONResponse.carauction response = JsonConvert.DeserializeObject<JSONResponse.carauction>(jsonString);



            //foreach (var item in response.data)
            //{
            //    if (item.auct_auctionid == auli_auctionid)
            //    {

            //        ViewBag.auct_nameth = item.auct_nameth;
            //        break;
            //    }
            //}

            return View();
        }

        public ActionResult banner()
        {
            try
            {
                var banner = db.banners.Select(x => new
                {
                    id = x.idbanner,
                    path = x.path,
                    status = x.status,
                    status_view = x.status_view,
                    URL = x.URL,
                    URL_upload = x.URL_upload,
                    name = x.name,
                }).ToList();

                return Json(banner, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("banner : " + ex.Message);
            }
        }

        public ActionResult data_about()
        {
            try
            {
                var about = db.abouts.Select(x => new
                {
                    id = x.idabout,
                    topic = x.topic,
                    textarea = x.textarea,
                }).ToList();
                return Json(about, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("data_about : " + ex.Message);
            }
        }
        public ActionResult data_about_head()
        {
            try
            {
                var about = db.about_head.Select(x => new
                {
                    id = x.idabout_head,
                    topic = x.topic,
                    textarea = x.textarea,
                    path = x.path,
                    status = x.status,
                }).ToList();
                return Json(about, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("data_about : " + ex.Message);
            }
        }
        public ActionResult data_howto()
        {
            try
            {
                var howto = db.howtoes.Select(x => new
                {
                    id = x.idhowto,
                    topic = x.topic,
                    textarea = x.textarea,
                }).ToList();
                return Json(howto, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("data_howto : " + ex.Message);
            }
        }
        public ActionResult data_news()
        {
            try
            {
                var news = db.news.OrderByDescending(x => x.date).Select(x => new
                {
                    id = x.idnews,
                    topic = x.topic,
                    path = x.path,
                    date = x.date,
                }).ToList();
                return Json(news, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("data_howto : " + ex.Message);
            }
        }
        public ActionResult select_news(int id)
        {
            try
            {
                var news = db.news.Where(t => t.idnews == id).Select(t => new
                {
                    id = t.idnews,
                    topic = t.topic,
                    path = t.path,
                    date = t.date,
                }).FirstOrDefault();

                var content = db.n_content.Where(x => x.id_news == id).Select(x => new
                {
                    id = x.idcontent,
                    id_news = id,
                    path = x.path,

                }).ToList();

                return Json(new { news, content }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("select_news : " + ex.Message);
            }
        }

        public ActionResult data_contact()
        {
            try
            {
                var content = db.contacts.Select(x => new
                {
                    id = x.idcontact,
                    office = x.office,
                    tel = x.tel,
                    fax = x.fax,
                    email = x.email,
                    address = x.address,
                    path = x.path,
                    google_map = x.google_map,
                    status = x.status,
                }).ToList();
                return Json(content, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("data_content : " + ex.Message);
            }
        }

        public ActionResult data_car_highlights()
        {
            try
            {
                //var car = db.car_highlights.Select(x => new
                //{
                //    id = x.idcar_highlights,
                //    name = x.name,
                //    path = x.path
                //}).ToList();

                HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(string.Format("http://devzt.devzt.me:81/Service/Get/highlight.asp"));
                WebReq.Method = "GET";

                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

                Console.WriteLine(WebResp.StatusCode);
                Console.WriteLine(WebResp.Server);

                string jsonString;
                using (Stream stream = WebResp.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8);
                    jsonString = reader.ReadToEnd();
                }
                JSONResponse.highlight response = JsonConvert.DeserializeObject<JSONResponse.highlight>(jsonString);

                foreach (var i in response.data)
                {
                    var view = db.viewcars.Where(x => x.id_car == i.high_carid.ToString()).FirstOrDefault();
                    if (view != null)
                    {
                        i.view = view.view.ToString();
                    }
                    else
                    {
                        i.view = "0";
                    }
                }


                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("data_car_highlights : " + ex.Message);
            }
        }

        public ActionResult data_auction()
        {
            try
            {
                HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(string.Format("http://devzt.devzt.me:81/Service/Get/auctionlist.asp"));
                WebReq.Method = "GET";

                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

                Console.WriteLine(WebResp.StatusCode);
                Console.WriteLine(WebResp.Server);

                string jsonString;
                using (Stream stream = WebResp.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8);
                    jsonString = reader.ReadToEnd();
                }
                JSONResponse.carauction response = JsonConvert.DeserializeObject<JSONResponse.carauction>(jsonString);

                return Json(response.data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("data_auction : " + ex.Message);
            }
        }
        public ActionResult data_calender()
        {
            try
            {
                var calender = db.calendars.OrderBy(x => x.date).Select(x => new
                {
                    id = x.idcalendar,
                    topic = x.topic,
                    date = x.date,
                    status = x.status,
                }).ToList();
                return Json(calender, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("data_calender : " + ex.Message);
            }
        }

        public ActionResult account_user(account_user account_User)
        {
            try
            {
                var message = 0;
                var username = db.account_user.Where(x => x.username == account_User.username).FirstOrDefault();
                var email = db.account_user.Where(x => x.email == account_User.email).FirstOrDefault();
                if (username == null)
                {
                    if (email == null)
                    {
                        db.account_user.Add(account_User);
                        db.SaveChanges();
                    }
                    else
                    {
                        message = 2; //email:fail
                    }
                }
                else
                {
                    message = 1; //username:fail
                }
                return Json(message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("account_user : " + ex.Message);
            }
        }

        public ActionResult GetCarTypeAPI()
        {
            try
            {
                HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(string.Format("http://devzt.devzt.me:81/Service/Get/cartype.asp"));
                WebReq.Method = "GET";

                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

                string jsonString;
                using (Stream stream = WebResp.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8);
                    jsonString = reader.ReadToEnd();
                }
                JSONResponse.cartype response = JsonConvert.DeserializeObject<JSONResponse.cartype>(jsonString);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("GetCarTypeAPI : " + ex.Message);
            }
        }

        public ActionResult Searchcard_license()
        {
            try
            {
                var text = Request["text"].ToString();
                var url = "http://devzt.devzt.me:81/Service/Get/cardetailsbook.asp?card_license=" + text;
                HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
                WebReq.Method = "GET";

                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

                string jsonString;
                using (Stream stream = WebResp.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8);
                    jsonString = reader.ReadToEnd();
                }
                return Json(jsonString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("GetCarTypeAPI : " + ex.Message);
            }
        }
        public ActionResult GetCarYearAPI()
        {
            try
            {
                HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(string.Format("http://devzt.devzt.me:81/Service/Get/caryear.asp"));
                WebReq.Method = "GET";

                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

                string jsonString;
                using (Stream stream = WebResp.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8);
                    jsonString = reader.ReadToEnd();
                }
                return Json(jsonString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("GetCarTypeAPI : " + ex.Message);
            }
        }

        public ActionResult GetCarlistAPI()
        {
            try
            {
                var card_brand_model = Request["card_brand_model"] == "false" || Request["card_brand_model"] == "ประเภทรถ" || Request["card_brand_model"] == null || Request["card_brand_model"] == "null" ? "0" : Request["card_brand_model"];
                var cartype = Request["cartype"] == "false" || Request["cartype"] == "ประเภทรถ" || Request["cartype"] == null || Request["cartype"] == "null" ? "0" : Request["cartype"];
                var caryear = Request["caryear"] == "false" || Request["caryear"] == "ปีรถ" || Request["caryear"] == null || Request["caryear"] == "null" ? "0" : Request["caryear"];
                var auli_auctionid = Request["auli_auctionid"] == "false" || Request["auli_auctionid"] == null || Request["auli_auctionid"] == "null" ? "0" : Request["auli_auctionid"];
                var card_carid = Request["card_carid"] == "false" || Request["card_carid"] == null ? "0" : Request["card_carid"];

                var API = "http://devzt.devzt.me:81/Service/Get/carlist.asp?card_brand_model=" + card_brand_model + "&card_type=" + cartype + "&card_year=" + caryear + "&auli_auctionid=" + auli_auctionid + "&card_carid=";
                HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(string.Format(API));
                WebReq.Method = "GET";

                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

                string jsonString;
                using (Stream stream = WebResp.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8);
                    jsonString = reader.ReadToEnd();
                }
                JSONResponse.carlist response = JsonConvert.DeserializeObject<JSONResponse.carlist>(jsonString);
                foreach (var i in response.data)
                {
                    var view = db.viewcars.Where(x => x.id_car == i.card_carid).FirstOrDefault();
                    if (view != null)
                    {
                        i.view = view.view.ToString();
                    }
                    else
                    {
                        i.view = "0";
                    }
                }

                var Iauli_auctionid = Convert.ToInt32(auli_auctionid);
                HttpWebRequest WebReq1 = (HttpWebRequest)WebRequest.Create(string.Format("http://devzt.devzt.me:81/Service/Get/auctionlist.asp"));
                WebReq.Method = "GET";

                HttpWebResponse WebResp1 = (HttpWebResponse)WebReq1.GetResponse();


                string jsonString1;
                using (Stream stream = WebResp1.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8);
                    jsonString1 = reader.ReadToEnd();
                }
                JSONResponse.carauction response1 = JsonConvert.DeserializeObject<JSONResponse.carauction>(jsonString1);


                var auct_nameth = "";
                var auct_date = "";
                foreach (var item in response1.data)
                {
                    if (item.auct_auctionid == Iauli_auctionid)
                    {

                        auct_nameth = item.auct_nameth;
                        auct_date = item.auct_auctiondate;
                        break;
                    }
                }

                return Json(new { response, auct_nameth, auct_date }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("GetCarTypeAPI : " + ex.Message);
            }
        }

        public ActionResult carauctiondetail()
        {
            var card_carid = Request["ID"] == "false" || Request["ID"] == null ? "0" : Request["ID"];

            var API = "http://devzt.devzt.me:81/Service/Get/carlist.asp?card_brand_model=0&card_type=0&card_year=0&auli_auctionid=0&card_carid=" + card_carid;
            HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(string.Format(API));
            WebReq.Method = "GET";

            HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

            Console.WriteLine(WebResp.StatusCode);
            Console.WriteLine(WebResp.Server);

            string jsonString;
            using (Stream stream = WebResp.GetResponseStream())
            {
                StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8);
                jsonString = reader.ReadToEnd();
            }

            var viewcar = db.viewcars.Where(x => x.id_car == card_carid).FirstOrDefault();
            if (viewcar != null)
            {
                //update
                viewcar.view = viewcar.view + 1;
                db.Entry(viewcar).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                //insert
                viewcar v = new viewcar();
                v.id_car = card_carid;
                v.view = 1;
                db.viewcars.Add(v);
                db.SaveChanges();
            }

            JSONResponse.carauctiondetail response = JsonConvert.DeserializeObject<JSONResponse.carauctiondetail>(jsonString);
            ViewBag.detail = response.data;

            var API_img = "http://devzt.devzt.me:81/Service/Get/carpicture.asp?card_carid=" + card_carid;
            WebReq = (HttpWebRequest)WebRequest.Create(string.Format(API_img));
            WebReq.Method = "GET";

            WebResp = (HttpWebResponse)WebReq.GetResponse();

            Console.WriteLine(WebResp.StatusCode);
            Console.WriteLine(WebResp.Server);

            using (Stream stream = WebResp.GetResponseStream())
            {
                StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8);
                jsonString = reader.ReadToEnd();
            }

            JSONResponse.carpicture response1 = JsonConvert.DeserializeObject<JSONResponse.carpicture>(jsonString);
            ViewBag.carpicture = response1.data;

            return View();
        }
    }
}