﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SahaCraneAuction.Models
{
    public class JSONResponse
    {
        public class carauction
        {
            public string status { get; set; }
            public List<CarAuction> data { get; set; }
        }

        public class highlight
        {
            public string status { get; set; }
            public List<Highlight> data { get; set; }
        }

        public class cartype
        {
            public string status { get; set; }
            public List<Cartype1> data { get; set; }
        }

        public class carlist
        {
            public string status { get; set; }
            public List<carlistdetail> data { get; set; }
        }
        public class carlistdetail
        {
            public string carp_front { get; set; }
            public string card_grade { get; set; }
            public string card_carid { get; set; }
            public string card_taxexpiredate { get; set; }
            public string lali_no { get; set; }
            public string card_brand { get; set; }
            public string card_model { get; set; }
            public string card_color { get; set; }
            public string card_year { get; set; }
            public string card_license { get; set; }
            public string card_province { get; set; }
            public string card_geartype { get; set; }
            public string card_miles { get; set; }
            public string card_cc { get; set; }
            public string grade { get; set; }
            public string card_machineno { get; set; }
            public string card_vin { get; set; }
            public string caau_aucstart { get; set; }
            public string caau_auctransfer { get; set; }
            public string caau_auctransport { get; set; }
            public string caau_auctax { get; set; }
            public string caau_aucaction { get; set; }
            public string card_remark { get; set; }
            public string view { get; set; }
            public string caau_aucotherservice { get; set; }


            public string auli_auctionid { get; set; }
            public string carp_rear { get; set; }

        }

        public class carauctiondetail
        {
            public string status { get; set; }
            public List<cardetail> data { get; set; }
        }

        public class cardetail
        {
            public string auli_auctionid { get; set; }
            public string card_brand { get; set; }
            public string card_model { get; set; }
            public string card_license { get; set; }
            public string card_machineno { get; set; }
            public string card_color { get; set; }
            public string card_vin { get; set; }
            public string caau_aucstart { get; set; }
            public string caau_auctransfer { get; set; }
            public string card_year { get; set; }
            public string caau_auctransport { get; set; }
            public string card_province { get; set; }
            public string caau_auctax { get; set; }
            public string card_geartype { get; set; }
            public string caau_aucotherservice { get; set; }
            public string card_miles { get; set; }
            public string caau_aucaction { get; set; }
            public string card_cc { get; set; }
            public string card_taxexpiredate { get; set; }
            public string carp_front { get; set; }
            public string carp_rear { get; set; }

            public string card_remark { get; set; }
            public string lali_no { get; set; }
            public string card_grade { get; set; }

        }

        public class carpicture
        {
            public string status { get; set; }
            public List<carpicturedetail> data { get; set; }
        }

        public class carpicturedetail
        {
            public string cpli_carid { get; set; }
            public string cpli_name { get; set; }
            public string cpli_img { get; set; }

        }


    }
}