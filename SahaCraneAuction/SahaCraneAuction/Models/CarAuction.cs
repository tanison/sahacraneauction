﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SahaCraneAuction.Models
{
    public class CarAuction
    {
        public int auct_auctionid { get; set; }
        public string auct_nameth { get; set; }
        public string auct_auctiondate { get; set; }
        public string auct_time { get; set; }
    }
}