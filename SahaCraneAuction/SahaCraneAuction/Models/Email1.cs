﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SahaCraneAuction.Models
{
    public class Email1
    {
        public string firstname_lastname { get; set; }
        public string tel { get; set; }
        public string email { get; set; }
        public string content_detail { get; set; }
    }
}