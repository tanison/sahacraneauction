﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SahaCraneAuction.Models
{
    public class Highlight
    {
        public int auct_auctionid { get; set; }
        public string auct_auctiontype { get; set; }
        public string carp_frontimg { get; set; }
        public int high_carid { get; set; }
        public string card_brand { get; set; }
        public string card_model { get; set; }
        public string view { get; set; }

    }
}