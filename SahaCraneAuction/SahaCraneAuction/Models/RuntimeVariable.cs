﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SahaCraneAuction.Models
{
    public class RuntimeVariable
    {
        public static account loginuser
        {
            get
            {
                return (account)HttpContext.Current.Session["loginuser"];
            }
            set
            {
                HttpContext.Current.Session["loginuser"] = value;
            }
        }
    }
}