<!doctype html>
<html lang="th">
<head>      
    <title>SAHA CRANE Auction</title>
    <?php require('inc_header.php'); ?>  
</head>
<body>
    <?php require('inc_navbar.php'); ?>
    <div class="bg-deepGray">
        <div class="container-fluid pb-3">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="resultSearch.php">ผลการค้นหารถเข้าประมูล</a></li>  
                <li class="breadcrumb-item active" aria-current="page">HONDA WAVE 110 i MT</li>
              </ol>
            </nav>
            <div class="container pb-4">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="text-white font-weight-medium text-center">ลำดับ 310 : HONDA WAVE 110 i MT</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-lightGgray">
        <div class="container py-5">
            <div id="sync1" class="owl-carousel owl-theme">
                <div class="item">
                    <img src="images/product/021964816a22b03f9ce888d9f91e7338.jpg" class="imgCurrentShow">
                </div>
                <div class="item">
                    <img src="images/product/03fd59c0619aa65319b6bad15741001c.jpg" class="imgCurrentShow">
                </div>
                <div class="item">
                    <img src="images/product/06af4593edc3b4e05b15de2573d2d56c.jpg" class="imgCurrentShow">
                </div>
                <div class="item">
                    <img src="images/product/8e2e351cf405d0ebc4c06d8f17e636af.jpg" class="imgCurrentShow">
                </div>
                <div class="item">
                    <img src="images/product/c9d98efa798bf7ec5e02db689d42c15e.jpg" class="imgCurrentShow">
                </div>
                <div class="item">
                    <img src="images/product/d4a314ebe8667ac3f071e5778e21ef38.jpg" class="imgCurrentShow">
                </div>
                <div class="item">
                    <img src="images/product/c9d98efa798bf7ec5e02db689d42c15e%20(1).jpg" class="imgCurrentShow">
                </div>
            </div>

            <div id="sync2" class="owl-carousel owl-theme mb-5">
                <div class="item">
                    <img src="images/product/021964816a22b03f9ce888d9f91e7338.jpg" class="thumbnailImg">
                </div>
                <div class="item">
                    <img src="images/product/03fd59c0619aa65319b6bad15741001c.jpg" class="thumbnailImg">
                </div>
                <div class="item">
                    <img src="images/product/06af4593edc3b4e05b15de2573d2d56c.jpg" class="thumbnailImg">
                </div>
                <div class="item">
                    <img src="images/product/8e2e351cf405d0ebc4c06d8f17e636af.jpg" class="thumbnailImg">
                </div>
                <div class="item">
                    <img src="images/product/c9d98efa798bf7ec5e02db689d42c15e.jpg" class="thumbnailImg">
                </div>
                <div class="item">
                    <img src="images/product/d4a314ebe8667ac3f071e5778e21ef38.jpg" class="thumbnailImg">
                </div>
                <div class="item">
                    <img src="images/product/c9d98efa798bf7ec5e02db689d42c15e%20(1).jpg" class="thumbnailImg">
                </div>
            </div>
            <script>
                $(document).ready(function() {
                var sync1 = $("#sync1");
                var sync2 = $("#sync2");
                var slidesPerPage = 6; //globaly define number of elements per page
                var syncedSecondary = true;

                sync1.owlCarousel({
                    items: 1,
                    slideSpeed: 2000,
                    nav: true,
                    autoplay: false, 
                    dots: false,
                    loop: true,
                    responsiveRefreshRate: 200,
                    navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
                }).on('changed.owl.carousel', syncPosition);

                sync2
                    .on('initialized.owl.carousel', function() {
                        sync2.find(".owl-item").eq(0).addClass("current");
                    })
                    .owlCarousel({
                        items: slidesPerPage,
                        dots: true,
                        nav: false,
                        smartSpeed: 200,
                        slideSpeed: 500,
                        slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
                        responsiveRefreshRate: 100,
                    responsive:{
                        0:{
                            items:3,
                        },
                        600:{
                            items:4,
                        },
                        1000:{
                            items:6,
                        }
                    }
                    }).on('changed.owl.carousel', syncPosition2);

                function syncPosition(el) {
                    //if you set loop to false, you have to restore this next line
                    //var current = el.item.index;

                    //if you disable loop you have to comment this block
                    var count = el.item.count - 1;
                    var current = Math.round(el.item.index - (el.item.count / 2) - .5);

                    if (current < 0) {
                        current = count;
                    }
                    if (current > count) {
                        current = 0;
                    }

                    //end block

                    sync2
                        .find(".owl-item")
                        .removeClass("current")
                        .eq(current)
                        .addClass("current");
                    var onscreen = sync2.find('.owl-item.active').length - 1;
                    var start = sync2.find('.owl-item.active').first().index();
                    var end = sync2.find('.owl-item.active').last().index();

                    if (current > end) {
                        sync2.data('owl.carousel').to(current, 100, true);
                    }
                    if (current < start) {
                        sync2.data('owl.carousel').to(current - onscreen, 100, true);
                    }
                }

                function syncPosition2(el) {
                    if (syncedSecondary) {
                        var number = el.item.index;
                        sync1.data('owl.carousel').to(number, 100, true);
                    }
                }

                sync2.on("click", ".owl-item", function(e) {
                    e.preventDefault();
                    var number = $(this).index();
                    sync1.data('owl.carousel').to(number, 300, true);
                });
            });
            </script>
            <div class="card card-body mb-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-7 col-md-8 col-lg-9 col-xl-10">
                                <h4 class="float-none float-md-left"><span class="d-block d-md-inline-block">ลำดับ 310 :</span> HONDA WAVE 110 i MT</h4>
                            </div>
                            <div class="col-5 col-md-4 col-lg-3 col-xl-2 text-right">
                                <a href="" class="btn btn-sm btn-warning float-none float-md-right"><i class="fas fa-file-pdf"></i> ดาวน์โหลด .pdf</a>  
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row border-bottom mb-0">
                                    <label for="inputPassword" class="col-6 col-md-5 col-lg-4 col-form-label font-weight-medium">ทะเบียน :</label>
                                    <div class="col-6 col-md-7 col-lg-8">
                                      <input type="text" readonly class="form-control-plaintext font-weight-medium" id="staticEmail" value="ขวก-791">
                                    </div>
                                </div>
                                <div class="form-group row border-bottom mb-0">
                                    <label for="inputPassword" class="col-6 col-md-5 col-lg-4 col-form-label font-weight-medium">สีรถ :</label>
                                    <div class="col-6 col-md-7 col-lg-8">
                                      <input type="text" readonly class="form-control-plaintext font-weight-medium" id="staticEmail" value="แดง">
                                    </div>
                                </div>
                                <div class="form-group row border-bottom mb-0">
                                    <label for="inputPassword" class="col-6 col-md-5 col-lg-4 col-form-label font-weight-medium">ยี่ห้อ :</label>
                                    <div class="col-6 col-md-7 col-lg-8">
                                      <input type="text" readonly class="form-control-plaintext font-weight-medium" id="staticEmail" value="HONDA">
                                    </div>
                                </div>
                                <div class="form-group row border-bottom mb-0">
                                    <label for="inputPassword" class="col-6 col-md-5 col-lg-4 col-form-label font-weight-medium">รุ่น :</label>
                                    <div class="col-6 col-md-7 col-lg-8">
                                      <input type="text" readonly class="form-control-plaintext font-weight-medium" id="staticEmail" value="WAVE 110 i">
                                    </div>
                                </div>
                                <div class="form-group row border-bottom mb-0">
                                    <label for="inputPassword" class="col-6 col-md-5 col-lg-4 col-form-label font-weight-medium">ปี :</label>
                                    <div class="col-6 col-md-7 col-lg-8">
                                      <input type="text" readonly class="form-control-plaintext font-weight-medium" id="staticEmail" value="2012">
                                    </div>
                                </div>
                                <div class="form-group row border-bottom mb-0">
                                    <label for="inputPassword" class="col-6 col-md-5 col-lg-4 col-form-label font-weight-medium">จังหวัด :</label>
                                    <div class="col-6 col-md-7 col-lg-8">
                                      <input type="text" readonly class="form-control-plaintext font-weight-medium" id="staticEmail" value="จ.ตรัง">
                                    </div>
                                </div>
                                <div class="form-group row border-bottom mb-0">
                                    <label for="inputPassword" class="col-6 col-md-5 col-lg-4 col-form-label font-weight-medium">ระบบเกียร์ :</label>
                                    <div class="col-6 col-md-7 col-lg-8">
                                      <input type="text" readonly class="form-control-plaintext font-weight-medium" id="staticEmail" value="MT">
                                    </div>
                                </div>
                                <div class="form-group row border-bottom mb-0">
                                    <label for="inputPassword" class="col-6 col-md-5 col-lg-4 col-form-label font-weight-medium">เลขไมล์ (KM) :</label>
                                    <div class="col-6 col-md-7 col-lg-8">
                                      <input type="text" readonly class="form-control-plaintext font-weight-medium" id="staticEmail" value="88,698">
                                    </div>
                                </div>
                                <div class="form-group row border-bottom mb-0">
                                    <label for="inputPassword" class="col-6 col-md-5 col-lg-4 col-form-label font-weight-medium">ซีซี :</label>
                                    <div class="col-6 col-md-7 col-lg-8">
                                      <input type="text" readonly class="form-control-plaintext font-weight-medium" id="staticEmail" value="110">
                                    </div>
                                </div>   
                                <div class="form-group row border-bottom mb-0">
                                    <label for="inputPassword" class="col-6 col-md-5 col-lg-4 col-form-label font-weight-medium">วันครบภาษี :</label>
                                    <div class="col-6 col-md-7 col-lg-8">
                                      <input type="text" readonly class="form-control-plaintext font-weight-medium" id="staticEmail" value="18/06/2020">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row border-bottom mb-0">
                                    <label for="staticEmail" class="col-6 col-md-5 col-lg-4 col-form-label font-weight-medium">เลขเครื่อง :</label>
                                    <div class="col-6 col-md-7 col-lg-8">
                                      <input type="text" readonly class="form-control-plaintext font-weight-medium" id="staticEmail" value="JA143E-0023017">
                                    </div>
                                </div>
                                <div class="form-group row border-bottom mb-0">
                                    <label for="inputPassword" class="col-6 col-md-5 col-lg-4 col-form-label font-weight-medium">เลขตัวถัง :</label>
                                    <div class="col-6 col-md-7 col-lg-8">
                                      <input type="text" readonly class="form-control-plaintext font-weight-medium" id="staticEmail" value="MLHJA1430B5023017">
                                    </div>
                                </div>
                                <div class="form-group row border-bottom mb-0">
                                    <label for="inputPassword" class="col-6 col-md-5 col-lg-4 col-form-label font-weight-medium">ราคาเปิดประมูล :</label>
                                    <div class="col-6 col-md-7 col-lg-8">
                                      <input type="text" readonly class="form-control-plaintext font-weight-medium text-danger" id="staticEmail" value="13,000">
                                    </div>
                                </div>
                                <div class="form-group row border-bottom mb-0">
                                    <label for="inputPassword" class="col-6 col-md-5 col-lg-4 col-form-label font-weight-medium">ค่าขนย้าย :</label>
                                    <div class="col-6 col-md-7 col-lg-8">
                                      <input type="text" readonly class="form-control-plaintext font-weight-medium text-danger" id="staticEmail" value="700">
                                    </div>
                                </div>
                                <div class="form-group row border-bottom mb-0">
                                    <label for="inputPassword" class="col-6 col-md-5 col-lg-4 col-form-label font-weight-medium">ค่าโอน :</label>
                                    <div class="col-6 col-md-7 col-lg-8">
                                      <input type="text" readonly class="form-control-plaintext font-weight-medium text-danger" id="staticEmail" value="-">
                                    </div>
                                </div>
                                <div class="form-group row border-bottom mb-0">
                                    <label for="inputPassword" class="col-6 col-md-5 col-lg-4 col-form-label font-weight-medium">ค่าภาษี :</label>
                                    <div class="col-6 col-md-7 col-lg-8">
                                      <input type="text" readonly class="form-control-plaintext font-weight-medium text-danger" id="staticEmail" value="-">
                                    </div>
                                </div>
                                <div class="form-group row border-bottom mb-0">
                                    <label for="inputPassword" class="col-6 col-md-5 col-lg-4 col-form-label font-weight-medium">ค่าอื่นๆ :</label>
                                    <div class="col-6 col-md-7 col-lg-8">
                                      <input type="text" readonly class="form-control-plaintext font-weight-medium text-danger" id="staticEmail" value="-">
                                    </div>
                                </div>
                                <div class="form-group row border-bottom mb-0">
                                    <label for="inputPassword" class="col-6 col-md-5 col-lg-4 col-form-label font-weight-medium">ค่าดำเนินการ :</label>
                                    <div class="col-6 col-md-7 col-lg-8">
                                      <input type="text" readonly class="form-control-plaintext font-weight-medium text-danger" id="staticEmail" value="1,500">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="alert alert-danger my-4 font-weight-medium" role="alert">
                          ***หมายเหตุ: ภาษีขาดต่อ ผู้ประมูลได้ดำเนินการเอง ไม่มี VAT
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php require('inc_footer.php'); ?>

</body>
</html>    